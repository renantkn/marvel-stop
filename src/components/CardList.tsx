import { Grid, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { useSelector } from "react-redux";
import { RootState } from "../store";

import { Character } from "../types/character";
import Card from "./Card";
import CardSkeleton from "./CardSkeleton";

const useStyles = makeStyles({
  cardList: {
    "& > div": {
      display: "flex",
      justifyContent: "center",
      padding: "16px 8px",
    },
  },
});

export default function CardList() {
  const classes = useStyles();

  const characters = useSelector((state: RootState) => state.characters.value);

  const isCharactersLoading = useSelector(
    (state: RootState) => state.isCharactersLoading.value
  );

  return (
    <Grid
      container
      sx={{ mb: 5 }}
      justifyContent="space-evenly"
      alignItems="flex-start"
      className={classes.cardList}
    >
      {isCharactersLoading ? (
        <>
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <CardSkeleton />
          </Grid>
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <CardSkeleton />
          </Grid>
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <CardSkeleton />
          </Grid>
        </>
      ) : characters.length > 0 ? (
        characters.map((character: Character) => (
          <Grid item xs={12} sm={6} md={4} lg={3} key={character.id}>
            <Card character={character} />
          </Grid>
        ))
      ) : (
        <Typography variant="h4">Nenhum Resultado</Typography>
      )}
    </Grid>
  );
}
