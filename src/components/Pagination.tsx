import React from "react";
import { Pagination as MuiPagination } from "@mui/material";
import { Box } from "@mui/system";
import { useDispatch, useSelector } from "react-redux";

import { setIsCharactersLoading } from "../features/character/isCharactersLoadingSlice";
import { setPagination } from "../features/pagination/paginationSlice";
import { AppDispatch, RootState } from "../store";
import { getNumberOfPages } from "../utils";

// Intervalo de tempo utilizado para não gerar delay quando o usuário troca as páginas rapidamente
let interval: NodeJS.Timeout;
const DELAY = 250;

export default function Pagination() {
  const page = useSelector((state: RootState) => state.pagination.value.page);
  const [currentPage, setCurrentPage] = React.useState(page);

  const dispatch = useDispatch<AppDispatch>();

  const { count, total } = useSelector(
    (state: RootState) => state.pagination.value
  );

  const numberOfPages = getNumberOfPages(total);

  React.useEffect(() => {
    setCurrentPage(page);
  }, [page]);

  const handleChange = (_: any, page: number) => {
    if (page === currentPage) return;

    clearInterval(interval);
    setCurrentPage(page);
    dispatch(setIsCharactersLoading(true));

    interval = setTimeout(() => {
      dispatch(setPagination({ count, page, total }));
    }, DELAY);
  };

  return numberOfPages > 0 ? (
    <Box display="flex" justifyContent="center">
      <MuiPagination
        page={currentPage}
        onChange={handleChange}
        count={numberOfPages}
        color="primary"
      />
    </Box>
  ) : null;
}
