import React from "react";
import { Alert, Button, Grid, Snackbar, TextField } from "@mui/material";
import { useFormik } from "formik";
import * as Yup from "yup";

import Storage from "../storage";
import { Character } from "../types/character";
import { concatImageExtension, splitImageExtension } from "../utils";

interface EditFormProps {
  character: Character;
}

export default function EditForm({ character }: EditFormProps) {
  const [submitSuccess, setSubmitSuccess] = React.useState(false);
  const { id, name, description, thumbnail } = character;

  const formik = useFormik({
    initialValues: {
      name,
      description,
      thumbnail: concatImageExtension(thumbnail),
    },

    validationSchema: Yup.object({
      name: Yup.string().required("Nome é um campo obrigatório"),
      description: Yup.string(),
      thumbnail: Yup.string().required("Imagem é um campo obrigatório"),
    }),

    onSubmit: () => {
      const FormData = (): Character => {
        return {
          id,
          modified: "",
          ...formik.values,
          thumbnail: splitImageExtension(formik.values.thumbnail),
        };
      };

      Storage.setCharacter(FormData());
      setSubmitSuccess(true);
    },
  });

  const handleClose = () => setSubmitSuccess(false);

  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid
        container
        spacing={1}
        sx={{
          p: 1,
          pl: 0,
          pt: 1,
          mb: 2,
          maxWidth: 540,
          minWidth: 350,
          margin: "auto",
        }}
        justifyContent="center"
      >
        <Grid item xs={12}>
          <TextField
            id="name"
            label="Nome"
            placeholder="Nome"
            onChange={formik.handleChange}
            value={formik.values.name}
            onBlur={formik.handleBlur}
            fullWidth
          />
          {formik.touched.name && formik.errors.name && (
            <Alert severity="error" sx={{ mt: 1, mb: 1 }}>
              {formik.errors.name}
            </Alert>
          )}
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="description"
            label="Descrição"
            placeholder="Descrição"
            onChange={formik.handleChange}
            value={formik.values.description}
            onBlur={formik.handleBlur}
            fullWidth
            multiline
            rows={5}
          />
          {formik.touched.description && formik.errors.description && (
            <Alert severity="error" sx={{ mt: 1, mb: 1 }}>
              {formik.errors.description}
            </Alert>
          )}
        </Grid>
        <Grid item xs={12}>
          <TextField
            id="thumbnail"
            label="Imagem"
            placeholder="Imagem"
            onChange={formik.handleChange}
            value={formik.values.thumbnail}
            onBlur={formik.handleBlur}
            fullWidth
          />
          {formik.touched.thumbnail && formik.errors.thumbnail && (
            <Alert severity="error" sx={{ mt: 1 }}>
              {formik.errors.thumbnail}
            </Alert>
          )}
        </Grid>
        <Button
          color="primary"
          variant="contained"
          fullWidth
          sx={{ mt: 2, ml: 1 }}
          type="submit"
        >
          Salvar
        </Button>
      </Grid>
      {submitSuccess && (
        <Snackbar
          open={submitSuccess}
          autoHideDuration={6000}
          onClose={handleClose}
          anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
        >
          <Alert severity="success">Dados salvos!</Alert>
        </Snackbar>
      )}
    </form>
  );
}
