import { useFormik } from "formik";
import { Search as SearchIcon } from "@mui/icons-material";
import {
  Button,
  Grid,
  InputAdornment,
  Paper,
  TextField,
  Typography,
} from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import * as Yup from "yup";

import { setCharacterName } from "../features/character/characterNameSlice";
import { setIsCharactersLoading } from "../features/character/isCharactersLoadingSlice";
import { setPagination } from "../features/pagination/paginationSlice";
import { AppDispatch, RootState } from "../store";

export default function CharacterForm() {
  const dispatch = useDispatch<AppDispatch>();

  const name = useSelector((state: RootState) => state.characterName.value);

  const { count, total } = useSelector(
    (state: RootState) => state.pagination.value
  );

  const resetPagination = () => {
    dispatch(setPagination({ count, page: 1, total }));
  };

  const formik = useFormik({
    initialValues: {
      name,
    },

    validationSchema: Yup.object({
      name: Yup.string(),
    }),

    onReset: () => {
      resetPagination();
      dispatch(setIsCharactersLoading(true));
      dispatch(setCharacterName(""));
    },

    onSubmit: () => {
      resetPagination();
      dispatch(setIsCharactersLoading(true));
      dispatch(setCharacterName(formik.values.name));
    },
  });

  return (
    <Paper elevation={1} sx={{ p: 3 }}>
      <form onSubmit={formik.handleSubmit}>
        <Grid container spacing={2} justifyContent="center">
          <Grid item>
            <TextField
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon />
                  </InputAdornment>
                ),
              }}
              id="name"
              label="Pesquisar"
              type="search"
              placeholder="Nome"
              size="small"
              onChange={formik.handleChange}
              value={formik.values.name}
            />
            {formik.errors.name ? (
              <Typography
                variant="caption"
                component="div"
                sx={{ color: "#D8000C" }}
              >
                {formik.errors.name}
              </Typography>
            ) : null}
          </Grid>
          <Grid item>
            <Button
              onClick={() => formik.handleSubmit()}
              variant="contained"
              sx={{ ml: 2 }}
            >
              Enviar
            </Button>
            <Button
              color="error"
              size="medium"
              variant="contained"
              sx={{ ml: 2 }}
              onClick={formik.handleReset}
            >
              Resetar
            </Button>
          </Grid>
        </Grid>
      </form>
    </Paper>
  );
}
