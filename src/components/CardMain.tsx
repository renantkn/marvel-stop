import { EventNote as EventNoteIcon } from "@mui/icons-material";
import {
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  Divider,
  Typography,
} from "@mui/material";
import { formatDate } from "../format";

import { Character } from "../types/character";
import { concatImageExtension } from "../utils";

interface CardMainProps {
  character: Character;
}

export default function CardMain({ character }: CardMainProps) {
  const { name, description, modified, thumbnail, series } = character;

  return (
    <Card sx={{ maxWidth: 550 }}>
      <CardHeader
        title={name}
        subheader={
          <>
            <EventNoteIcon fontSize="inherit" /> {formatDate(modified, "long")}
          </>
        }
      />
      <CardMedia
        component="img"
        height={350}
        image={concatImageExtension(thumbnail)}
        alt={name}
      />
      <CardContent>
        {!!description && (
          <Typography variant="body2" color="text.secondary" gutterBottom>
            {description}
          </Typography>
        )}
        {series?.items?.length! > 0 && (
          <>
            <Divider sx={{ mt: 2, mb: 2 }} />
            <Typography variant="body1" gutterBottom>
              Series:
            </Typography>
            {series?.items.map((item) => (
              <Typography variant="body2" key={item.name}>
                - {item.name}
              </Typography>
            ))}
          </>
        )}
      </CardContent>
    </Card>
  );
}
