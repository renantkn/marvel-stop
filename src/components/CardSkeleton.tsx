import { Card, CardContent, Skeleton } from "@mui/material";

export default function CardSkeleton() {
  return (
    <Card sx={{ width: "100%", maxWidth: 350 }}>
      <Skeleton sx={{ height: 250 }} animation="wave" variant="rectangular" />
      <CardContent>
        <Skeleton
          animation="wave"
          height={30}
          width={150}
          sx={{ marginBottom: 1 }}
        />
        <Skeleton
          animation="wave"
          height={10}
          width={200}
          sx={{ marginBottom: 2 }}
        />
        <Skeleton animation="wave" height={10} sx={{ marginBottom: 1 }} />
        <Skeleton animation="wave" height={10} sx={{ marginBottom: 1 }} />
      </CardContent>
    </Card>
  );
}
