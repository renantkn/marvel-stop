import React from "react";
import { EventNote as EventNoteIcon } from "@mui/icons-material";
import {
  Button,
  Card as MuiCard,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from "@mui/material";
import { truncate } from "lodash";

import { formatDate } from "../format";
import { Character } from "../types/character";
import { concatImageExtension } from "../utils";
import { Link } from "react-router-dom";

interface CharacterProps {
  character: Character;
}

export default function Card({ character }: CharacterProps) {
  const [truncateDescription, setTruncateDescription] = React.useState(true);

  const { id, name, description, modified, thumbnail } = character;

  const toggleTruncateDescription = () =>
    setTruncateDescription(!truncateDescription);

  const truncateLength = 60;

  return (
    <MuiCard sx={{ width: "100%", maxWidth: 350 }}>
      <CardMedia
        component="img"
        height={250}
        image={concatImageExtension(thumbnail)}
        alt={name}
      />
      <CardContent>
        <Typography gutterBottom variant="body1" component="div">
          {name}
        </Typography>
        <Typography
          gutterBottom
          variant="caption"
          component="div"
          sx={{ color: "#6c757d" }}
        >
          <EventNoteIcon fontSize="inherit" /> {formatDate(modified)}
        </Typography>
        {!!description && (
          <>
            <Typography variant="body2" component="div" color="text.secondary">
              {truncateDescription
                ? truncate(description, { length: truncateLength })
                : description}
              {description.length > truncateLength && (
                <Button size="small" onClick={toggleTruncateDescription}>
                  {truncateDescription ? "Ler mais" : "Ler menos"}
                </Button>
              )}
            </Typography>
          </>
        )}
      </CardContent>
      <CardActions>
        <Button size="small" component={Link} to={`/edit/${id}`}>
          Editar
        </Button>
        <Button size="small" component={Link} to={`/character/${id}`}>
          Ver detalhes
        </Button>
      </CardActions>
    </MuiCard>
  );
}
