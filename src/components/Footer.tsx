import { Link } from "@mui/material";
import { blue } from "@mui/material/colors";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles({
  footer: {
    backgroundColor: blue[500],
    position: "fixed",
    bottom: 0,
    width: "100%",
    padding: 4,
    color: "#fff",
    textAlign: "center",
    fontSize: 13,

    "& a": {
      color: "inherit",
      textDecoration: "none",
      fontWeight: 700,
    },
  },
});

export default function Footer() {
  const classes = useStyles();

  return (
    <div className={classes.footer}>
      Project by{" "}
      <Link
        href="https://gitlab.com/renantkn/marvel-stop"
        color="inherit"
        target="_blank"
      >
        RenanTKN
      </Link>{" "}
      | Data provided by{" "}
      <Link href="https://developer.marvel.com/docs" target="_blank">
        Marvel
      </Link>
      . &copy; 2014 Marvel
    </div>
  );
}
