import { Character } from "../types/character";

const editedCharacters = "editedCharacters";

class Storage {
  setCharacter = ({ id, name, description, thumbnail }: Character) => {
    localStorage.setItem(
      editedCharacters,
      JSON.stringify({
        ...this.getCharacters(),
        [id]: { name, description, thumbnail },
      })
    );
  };

  getCharacters = () =>
    JSON.parse(localStorage.getItem(editedCharacters)!) ?? {};

  getCharacter = (id: string) => this.getCharacters()[id];
}

export default new Storage();
