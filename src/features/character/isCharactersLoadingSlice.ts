import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface isCharactersLoadingState {
  value: boolean;
}

const initialState: isCharactersLoadingState = {
  value: true,
};

export const isCharactersLoadingSlice = createSlice({
  name: "isCharactersLoading",
  initialState,
  reducers: {
    setIsCharactersLoading: (state, action: PayloadAction<boolean>) => {
      state.value = action.payload;
    },
  },
});

export const { setIsCharactersLoading } = isCharactersLoadingSlice.actions;

export default isCharactersLoadingSlice.reducer;
