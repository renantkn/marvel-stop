import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface characterNameState {
  value: string;
}

const initialState: characterNameState = {
  value: "",
};

export const characterNameSlice = createSlice({
  name: "characterName",
  initialState,
  reducers: {
    setCharacterName: (state, action: PayloadAction<string>) => {
      state.value = action.payload;
    },
  },
});

export const { setCharacterName } = characterNameSlice.actions;

export default characterNameSlice.reducer;
