import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { Character } from "../../types/character";

interface charactersState {
  value: Character[];
}

const initialState: charactersState = {
  value: [],
};

export const charactersSlice = createSlice({
  name: "characters",
  initialState,
  reducers: {
    loadCharacters: (state, action: PayloadAction<Character[]>) => {
      state.value = action.payload;
    },
  },
});

export const { loadCharacters } = charactersSlice.actions;

export default charactersSlice.reducer;
