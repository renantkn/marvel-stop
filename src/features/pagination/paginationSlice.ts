import { createSlice, PayloadAction } from "@reduxjs/toolkit";

import { Pagination } from "../../types/pagination";

interface paginationState {
  value: Pagination;
}

const initialState: paginationState = {
  value: { count: 0, page: 1, total: 0 },
};

export const paginationSlice = createSlice({
  name: "pagination",
  initialState,
  reducers: {
    setPagination: (state, action: PayloadAction<Pagination>) => {
      state.value = action.payload;
    },
  },
});

export const { setPagination } = paginationSlice.actions;

export default paginationSlice.reducer;
