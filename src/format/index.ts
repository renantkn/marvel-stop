type month = "numeric" | "long";

export const formatDate = (date: string, month: month = "numeric") =>
  new Date(date).toLocaleDateString("pt-BR", {
    hour: "numeric",
    minute: "numeric",
    second: "numeric",
    day: "numeric",
    month,
    year: "numeric",
  });
