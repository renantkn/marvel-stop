import { API_LIMIT } from "../constants";
import { CharacterThumbnail } from "../types/character";

export const concatImageExtension = ({ path, extension }: CharacterThumbnail) =>
  `${path}.${extension}`;

export const splitImageExtension = (filename: string): CharacterThumbnail => {
  const index = filename.lastIndexOf(".");
  const path = filename.substring(0, index);
  const extension = filename.substring(index + 1);

  return { path, extension };
};

export const getNumberOfPages = (total: number) => Math.ceil(total / API_LIMIT);
