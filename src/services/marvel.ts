import axios from "axios";
import { find } from "lodash";

import { API_LIMIT } from "../constants";
import { setPagination } from "../features/pagination/paginationSlice";
import Storage from "../storage";
import { store } from "../store";
import { Pagination } from "../types/pagination";

const apikey = process.env.REACT_APP_MARVEL_API_KEY;

const instance = axios.create({
  baseURL: "https://gateway.marvel.com/v1/public",
  params: { apikey },
});

class Marvel {
  #getOffset = (page: number) => (page - 1) * API_LIMIT;

  #updatePagination = ({ count, total }: { count: number; total: number }) => {
    const { page } = store.getState().pagination.value;
    const pagination: Pagination = { count, page, total };
    store.dispatch(setPagination(pagination));
  };

  #applyEdits = (data: any) => {
    const characterList = data?.data?.results as any[];
    const editedCharacters = Storage.getCharacters();
    Object.keys(editedCharacters).map((id: string) => {
      const currentCharacter = Storage.getCharacter(id);
      const editedCharacter = find(characterList, { id: parseInt(id) });

      editedCharacter.name = currentCharacter.name;
      editedCharacter.description = currentCharacter.description;
      editedCharacter.thumbnail = currentCharacter.thumbnail;

      return editedCharacter;
    });
    return data;
  };

  getCharacters = async (page = 1) => {
    const name = store.getState().characterName.value;
    const queryName = !!name ? `&nameStartsWith=${name}` : "";

    return await instance
      .get(
        `/characters?limit=${API_LIMIT}&offset=${this.#getOffset(
          page
        )}${queryName}`
      )
      .then((res) => {
        this.#updatePagination(res?.data.data);
        return !!name ? res?.data : this.#applyEdits(res?.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  getCharacter = async (id: string) =>
    await instance
      .get(`/characters/${id}`)
      .then((res) => res?.data)
      .catch((e) => {
        console.log(e);
      });
}

export default new Marvel();
