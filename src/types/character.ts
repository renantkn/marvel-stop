export interface CharacterThumbnail {
  path: string;
  extension: string;
}

export interface CharacterSeries {
  available: number;
  items: {
    resourceURI: "string";
    name: "string";
  }[];
}

export interface Character {
  id: number;
  name: string;
  description?: string;
  modified: string;
  thumbnail: CharacterThumbnail;
  series?: CharacterSeries;
}
