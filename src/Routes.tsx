import { Routes as RoutesList, Route } from "react-router-dom";

import Home from "./pages/Home";
import Character from "./pages/Character";
import EditCharacter from "./pages/EditCharacter";
import About from "./pages/About";

export default function Routes() {
  return (
    <RoutesList>
      <Route path="/" element={<Home />} />
      <Route path="/character/:id" element={<Character />} />
      <Route path="/edit/:id" element={<EditCharacter />} />
      <Route path="/about" element={<About />} />
    </RoutesList>
  );
}
