import {
  Home as HomeIcon,
  Info as InfoIcon,
} from "@mui/icons-material";

interface menuItemsProps {
  text: string;
  icon: JSX.Element;
  path: string;
}

export const menuItems: menuItemsProps[] = [
  {
    text: "Home",
    icon: <HomeIcon />,
    path: "/",
  },
  {
    text: "About",
    icon: <InfoIcon />,
    path: "/about",
  },
];
