import { Grid } from "@mui/material";
import React from "react";
import { useParams } from "react-router";
import CardMain from "../components/CardMain";

import Marvel from "../services/marvel";
import { Character as CharacterType } from "../types/character";
import Storage from "../storage";

export default function Character() {
  const { id } = useParams();
  const [character, setCharacter] = React.useState<CharacterType>({
    id: 0,
    name: "",
    description: "",
    modified: "",
    thumbnail: {
      path: "",
      extension: "",
    },
  });

  React.useEffect(() => {
    if (!!id) {
      const editedCharacter = Storage.getCharacter(id);

      Marvel.getCharacter(id).then((data) => {
        editedCharacter
          ? setCharacter({
              ...data.data?.results[0],
              name: editedCharacter.name,
              description: editedCharacter.description,
              thumbnail: editedCharacter.thumbnail,
            })
          : setCharacter(data.data?.results[0]);
      });
    }
  }, [id]);

  return (
    <Grid container justifyContent="center" sx={{ mt: 3, mb: 3 }}>
      <Grid item>
        {!!character?.name && <CardMain character={character} />}
      </Grid>
    </Grid>
  );
}
