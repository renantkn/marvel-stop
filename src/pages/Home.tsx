import { Container, Typography } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
import { useDispatch, useSelector } from "react-redux";

import CardList from "../components/CardList";
import CharacterForm from "../components/CharacterForm";
import Pagination from "../components/Pagination";
import { loadCharacters } from "../features/character/charactersSlice";
import { setIsCharactersLoading } from "../features/character/isCharactersLoadingSlice";
import { setPagination } from "../features/pagination/paginationSlice";
import Marvel from "../services/marvel";
import { AppDispatch, RootState } from "../store";
import { Pagination as PaginationType } from "../types/pagination";

export default function Home() {
  const dispatch = useDispatch<AppDispatch>();

  const page = useSelector((state: RootState) => state.pagination.value.page);
  const name = useSelector((state: RootState) => state.characterName.value);

  React.useEffect(() => {
    dispatch(setIsCharactersLoading(true));
    Marvel.getCharacters(page).then((res) => {
      try {
        const { count, total } = res?.data;
        const pagination: PaginationType = { count, page, total };

        if (!!count && !!total) {
          dispatch(setPagination(pagination));
          dispatch(loadCharacters(res.data.results));
          dispatch(setIsCharactersLoading(false));
        }
      } catch (e) {
        console.log(e);
      }
    });
  }, [page, name, dispatch]);

  return (
    <Container maxWidth="lg">
      <Typography
        variant="h5"
        sx={{ m: 3, display: "flex", justifyContent: "center" }}
      >
        Personagens
      </Typography>
      <CharacterForm />
      <Box mt={3} mb={1}>
        <Pagination />
      </Box>
      <CardList />
      <Box mb={5}>
        <Pagination />
      </Box>
    </Container>
  );
}
