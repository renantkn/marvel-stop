import { Container, Link as MuiLink, Typography } from "@mui/material";

const Link = ({ children, url }: { children: string; url: string }) => (
  <MuiLink
    href={url}
    target="_blank"
    color="inherit"
    sx={{ textDecoration: "none", fontWeight: 700 }}
  >
    {children}
  </MuiLink>
);

export default function About() {
  return (
    <Container maxWidth="md">
      <Typography variant="h3" gutterBottom sx={{ m: 3, textAlign: "center" }}>
        Marvel Stop
      </Typography>

      <Typography variant="body1">
        Projeto que utiliza a API da{" "}
        <Link url="https://developer.marvel.com/docs">Marvel</Link> para listar
        os personagens.
      </Typography>
      <Typography variant="body1" component="div" sx={{ mt: 3 }}>
        Demo online no site{" "}
        <Link url="https://marvel-stop.vercel.app/">
          https://marvel-stop.vercel.app/
        </Link>{" "}
        e código-fonte em{" "}
        <Link url="https://gitlab.com/renantkn/marvel-stop">
          https://gitlab.com/renantkn/marvel-stop
        </Link>
        .
      </Typography>
      <Typography variant="body1" component="div" sx={{ mt: 3 }}>
        <strong>Funcionalidades:</strong>
        <ul>
          <li>Buscar pelo nome do personagem</li>
          <li>Ver detalhes de um personagem (inclusive a lista de séries)</li>
          <li>
            Editar um personagem (alteração ficará salva apenas no client-side)
          </li>
        </ul>
      </Typography>
      <Typography variant="body1" component="div" sx={{ mt: 3 }}>
        <strong>Informações Técnicas:</strong>
        <ul>
          <li>
            Projeto feito com o{" "}
            <Link url="https://create-react-app.dev/">create-react-app</Link>
          </li>
          <li>
            <Link url="https://redux.js.org/">Redux</Link> para gerenciar
            estados
          </li>
          <li>
            <Link url="https://reactrouter.com/">React-router</Link> para criar
            as rotas
          </li>
          <li>
            <Link url="https://testing-library.com/">
              @testing-library/react
            </Link>{" "}
            para fazer os testes
          </li>
          <li>Pipeline para build, test e deploy</li>
        </ul>
      </Typography>
    </Container>
  );
}
