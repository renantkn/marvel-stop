import React from "react";
import { Edit as EditIcon } from "@mui/icons-material";
import { Typography } from "@mui/material";
import { useParams } from "react-router";

import EditForm from "../components/EditForm";
import Marvel from "../services/marvel";
import { Character } from "../types/character";

export default function EditCharacter() {
  const { id } = useParams();
  const [character, setCharacter] = React.useState<Character>({
    id: 0,
    name: "",
    description: "",
    modified: "",
    thumbnail: {
      path: "",
      extension: "",
    },
  });

  React.useEffect(() => {
    if (!!id) {
      Marvel.getCharacter(id).then((data) => {
        setCharacter(data.data?.results[0]);
      });
    }
  }, [id]);

  return !!character?.name ? (
    <>
      <Typography variant="h5" align="center" gutterBottom sx={{ m: 3 }}>
        <EditIcon /> Editar
      </Typography>
      <EditForm character={character} />
    </>
  ) : null;
}
