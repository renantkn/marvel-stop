import { Character } from "../types/character";

const mockedCharacters: Character[] = [
  {
    id: 1,
    name: "Super Mock",
    description: "foo description",
    modified: "2013-09-18T10:16:10-0400",
    thumbnail: {
      path: "http://i.annihil.us/u/prod/marvel/i/mg/9/03/5239b59f49020",
      extension: "jpg",
    },
  },
];

const randomCharacter = (id: number): Character => ({
  id,
  name: "foo",
  modified: "2013-09-18T10:16:10-0400",
  thumbnail: {
    path: "http://i.annihil.us/u/prod/marvel/i/mg/9/03/5239b59f49020",
    extension: "jpg",
  },
});

for (let i in [...Array(99).keys()]) {
  mockedCharacters.push(randomCharacter(+i + 2));
}

mockedCharacters.push({
  id: 101,
  name: "Spider Mock",
  modified: "2013-09-18T10:16:10-0400",
  thumbnail: {
    path: "http://i.annihil.us/u/prod/marvel/i/mg/9/03/5239b59f49020",
    extension: "jpg",
  },
});

export default mockedCharacters;
