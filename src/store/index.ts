import { configureStore } from "@reduxjs/toolkit";

import charactersReducer from "../features/character/charactersSlice";
import isCharactersLoadingReducer from "../features/character/isCharactersLoadingSlice";
import paginationReducer from "../features/pagination/paginationSlice";
import characterNameReducer from "../features/character/characterNameSlice";

export const store = configureStore({
  reducer: {
    characters: charactersReducer,
    isCharactersLoading: isCharactersLoadingReducer,
    pagination: paginationReducer,
    characterName: characterNameReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
