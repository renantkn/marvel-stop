import { render, waitFor, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";

import { setCharacterName } from "../features/character/characterNameSlice";
import { loadCharacters } from "../features/character/charactersSlice";
import { setIsCharactersLoading } from "../features/character/isCharactersLoadingSlice";
import { setPagination } from "../features/pagination/paginationSlice";
import mockedCharacters from "../mock/characters";
import Home from "../pages/Home";
import { store } from "../store";

beforeAll(() => {
  store.dispatch(loadCharacters(mockedCharacters));
});

test("Check if character is on the list", async () => {
  render(
    <Provider store={store}>
      <BrowserRouter>
        <Home />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch(setIsCharactersLoading(false));

  await waitFor(() => screen.getByText("Super Mock"));

  expect(screen.getByText("Super Mock")).toBeInTheDocument();
});

test("Check pagination and query name", async () => {
  render(
    <Provider store={store}>
      <BrowserRouter>
        <Home />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch(setPagination({ count: 100, page: 2, total: 101 }));
  store.dispatch(setCharacterName("Spider"));
  store.dispatch(setIsCharactersLoading(false));

  await waitFor(() => screen.getByText("Spider Mock"));

  expect(screen.getByText("Spider Mock")).toBeInTheDocument();
});
