import Footer from "./components/Footer";
import Navbar from "./components/Navbar";
import Routes from "./Routes";

const App = () => (
  <>
    <Navbar />
    <Routes />
    <Footer />
  </>
);

export default App;
