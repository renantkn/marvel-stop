# Marvel Stop

Projeto que utiliza a API da
[Marvel](https://developer.marvel.com/docs) para listar os personagens.

## Demo online

- [https://marvel-stop.vercel.app/](https://marvel-stop.vercel.app/)

![Marvel Stop](./assets/marvel-stop.gif)

## Funcionalidades

- Buscar pelo nome do personagem
- Ver detalhes de um personagem (inclusive a lista de séries)
- Editar um personagem (alteração ficará salva apenas no client-side)

## Informações Técnicas

- Projeto feito com o [create-react-app](https://create-react-app.dev/)
- [Redux](https://redux.js.org/) para gerenciar estados -[React-router](https://reactrouter.com/) para criar as rotas
- [@testing-library/react](https://testing-library.com/) para fazer os testes
- Pipeline para build, test e deploy
